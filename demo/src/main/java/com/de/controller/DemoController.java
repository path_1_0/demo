package com.de.controller;

import com.de.service.DemoService;
import com.de.vo.Demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("demo")
@RestController
@CrossOrigin
public class DemoController {
    @Autowired
    private DemoService demoService;

    @GetMapping("/t")
    public List<Demo> tree(){
        List<Demo> list=demoService.tree();


        return list;
    }
}
