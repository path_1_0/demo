package com.de.controller;

import com.de.service.ActivityService;
import com.de.vo.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/activity")
@CrossOrigin
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    @GetMapping("/g")
    public List<Activity> get(){
        List<Activity> list=activityService.select();
        return list;
    }
    @PostMapping("/s")
    public Activity get(@PathVariable Integer id){
        Activity activity=activityService.selectById(id);
        return activity;
    }
    @PostMapping("/i")
    public void insert(Activity activity){
        activityService.add(activity);
    }
    @PostMapping ("/d/{id}")
    public void delete( @PathVariable  Integer id){
        activityService.delect(id);
    }
    @PostMapping("/u")
    public void update(Activity activity){

        activityService.updateById(activity);
    }
}