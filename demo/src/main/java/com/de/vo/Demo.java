package com.de.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.awt.*;
import java.util.List;

@TableName("demo")
public class Demo {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Integer pId;
//    @JsonProperty("text")
    private String name;
    @TableField(exist = false)
    private List<Demo> children;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Demo> getChildren() {
        return children;
    }

    public void setChildren(List<Demo> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "Demo{" +
                "id=" + id +
                ", pId=" + pId +
                ", name='" + name + '\'' +
                ", children=" + children +
                '}';
    }
}
