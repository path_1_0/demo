package com.de.service;

import com.de.mapper.ActivityMapper;
import com.de.vo.Activity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService{

    @Autowired
    private ActivityMapper activityMapper;

    public List<Activity> select() {

        return activityMapper.selectList(null);
    }


    public void updateById(Activity activity) {
        activityMapper.updateById(activity);
    }


    public void delect(Integer id) {
        activityMapper.deleteById(id);
    }


    public void add(Activity activity) {
        activityMapper.insert(activity);
    }


    public Activity selectById(Integer id) {
        return activityMapper.selectById(id);
    }


}
