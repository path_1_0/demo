package com.de.service;

import com.de.vo.Demo;

import java.util.List;

public interface DemoService {
      List<Demo> tree();
}
