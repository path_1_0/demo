package com.de.service;

import com.de.vo.Activity;

import java.util.List;

public interface ActivityService {
    List<Activity> select();

    void updateById(Activity activity);

    void delect(Integer id);

    void add(Activity activity);

    Activity selectById(Integer id);
}
