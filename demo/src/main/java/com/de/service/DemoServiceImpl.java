package com.de.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.de.mapper.DemoMapper;
import com.de.vo.Demo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class DemoServiceImpl implements DemoService{
    @Autowired
    private DemoMapper demoMapper;




    public void tree(List<Demo> list,List<Demo> listresult){
        for(Demo demoresult : listresult){
            List<Demo> arrayList = new ArrayList();
            for(Demo demo:list){
                if(demo.getpId()==0){
                    continue;
                }
                if (demoresult.getId()==demo.getpId()){
                    arrayList.add(demo);
                }
                demoresult.setChildren(arrayList);
                if(!demoresult.getChildren().isEmpty()){
                    tree(list,demoresult.getChildren());
                }
            }
        }
    }

    public List<Demo> tree() {
        List<Demo> list = demoMapper.selectList(null);
        List<Demo> listone=new ArrayList();
        for(Demo demo: list){
            if(demo.getpId()==0){
                listone.add(demo);
            }
        }
        tree(list,listone);
        return listone;
    }
}
