package com.de.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.de.vo.Demo;
public interface DemoMapper extends BaseMapper<Demo> {
}
