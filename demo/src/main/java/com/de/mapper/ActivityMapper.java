package com.de.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.de.vo.Activity;

public interface ActivityMapper extends BaseMapper<Activity> {
}
